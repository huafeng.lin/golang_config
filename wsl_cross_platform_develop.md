# Windows Cross Platform Develop Linux apps using WSL

The Windows Subsystem for Linux (**WSL**) lets developers run a GNU/Linux environment -- including most command-line tools, utilities, and applications -- directly on Windows, unmodified, without the overhead of a virtual machine. It is necessary to test your code in WSL before you submit them into server.  

## Install WSL

1. Search and Install Linux system in Microsoft Store, please reference [this link](https://docs.microsoft.com/en-us/windows/wsl/install-win10) if you has issue in install **WSL**. For me, I install Ubuntu 18.04 LTS linux system.

2. Open the **WSL** (Ubuntu) and config the Golang environment reference [Setup Go Development Environment with VS Code and WSL on Windows](https://medium.com/@betakuang/setup-go-development-environment-with-vs-code-and-wsl-on-windows-62bd4625c6a7).

    * After settingthe **GOROOT** and **GOPATH** in the *.profile* file, please refresh profile by running:

        ``` bash
        source ~/.profile
        ```

3. install Code Runner extensions to run go application instead of command line.
![Img1](assets/wsl_cross_platform_develop/1_code_runner.png)
