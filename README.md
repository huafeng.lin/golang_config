# Golang

This document is used to introduce how to config the golang + VSCode develop environment in Windows. In addtional, it also guides windows users to do the cross-platform develop linux golang application using WSL in the VSCode.

*[Config Golang and VSCode for Windows](config_golang_windows.md)

*[Windows Cross Platform Develop Linux apps using WSL](wsl_cross_platform_develop.md)
