# Config Golang + VSCode developer environment for Windows

## Config Golang

1. Download and install the latest version of Golang from the link [Golang](https://golang.org/dl/).

   Noted: please confirm that your golang version mores than **1.13**.

2. Set the ****GOROOT**** and ****GOPATH****.

   The ****GOROOT**** points to the directory in which golang was installed, the default folder is *"C:\Go\"*.

   The ****GOPATH**** is set to get, build and install packages outside the standard Go tree, the default folder is set as *"C:\Users\  %UserName%\go"* in user Variables after you install the Golang.
   
     ![Img1](assets/config_golang_windows/1_user_gopath.png)

   * Set the **GOROOT** and **GOPATH** in system Environment Variables.  
  
     ![Img2](assets/config_golang_windows/2_system_gopath_goroot.png)

   * **GOROOT** and **GOPATH** can't be set as same folder.
   * Better to add their "bin" subfolder into Path.
   
     ![Img3](assets/config_golang_windows/3_set_path.png)

## Config VSCode

1. Download and install the latest version of VSCode from the link [VSCode](https://code.visualstudio.com/).

2. Install the Go extension in VSCode.
3. 
    ![Img4](assets/config_golang_windows/4_install_go_extension.png)
3. Create folder to store the Golang Code and create the "bin", "pkg" and "src" subfolder.

4. enter into the "src" folder and create a new file named "main.go".

5. open the main.go file using VSCode.

6. VSCode will remind you to install the Go tools and select "install all".
7. 
    ![Img5](assets/config_golang_windows/5_install_go_tools.png)

7. you can find 17 tools in your **GOPATH**.
8. 
    ![Img6](assets/config_golang_windows/6_gotools_list.png)

## First Go programe

1. Please try to paste the following code into the "main.go".

    ```Go
   package main

   import "fmt"

    func main() {
        fmt.Println("hello world")
    }
    ```

2. Press "F5" (select "Debug"->"Start Debugging") to run the programe and you can see the "hello world" is print in "DEBUG CONSOLE".

## First Go Package

In this section, we introduce how to write and use the self-define package. Normally, all go code should be set in the "src" folder. Each pakcage has a special folder and the package name is same with folder name.

1. create a sub folder named "test" in "src" folder.

2. create a file named "testPrintln.go".

    ![Img7](assets/config_golang_windows/7_test_println.png)

3. the first character of function name have to upper if you want to user this function in other package.

4. better to add the comment for all exportable function.

5. the VSCode will remind you can't find the "test" pakcage when you try to import and call the self-define package into main.go.
   
   ![Img8](assets/config_golang_windows/8_self_defined_package.png)

6. you can add current project folder into **GOPATH** to fix this issue. Or, you set the special **GOPATH** for this project in VSCode.
   * select "File" -> "Preferences" -> "Setting"->"Wrokspace"
    
    ![Img9](assets/config_golang_windows/9_edit_setting.png)

   * click the edit the "settings.json" and VSCode will create the ".vscode" folder and "{}setting.json" file.
   * add the go.gopath variable and rerun the programe.
    
    ![Img10](assets/config_golang_windows/10.edit_gopath.png)
